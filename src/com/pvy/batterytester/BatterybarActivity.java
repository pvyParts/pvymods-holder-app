package com.pvy.batterytester;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class BatterybarActivity extends Activity {
    /** Called when the activity is first created. */
	private int width;
	private int height;
	private int batteryLevel = 5;
	private int batteryStatus;
	private boolean mAttached;
	private String prependText = "";
	private String appendText = "%";
    //private float textsize = getTextSize();
    
	private static final int STYLE_SHOW = 1;
	private static final int STYLE_DISABLE = 2;

	private int style;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        final TextView bar = (TextView) findViewById(R.id.bar);
    	Button update = (Button) findViewById(R.id.button1);
    	EditText textbar = (EditText) findViewById(R.id.editText1);
    	
    	update.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				batteryLevel += 1;
				ViewGroup.LayoutParams baseparam = bar.getLayoutParams();
				width = 480;
				height = 1;
				Log.d("battery", width+" "+height);
				int batdev = width / 100;
				baseparam.width = batdev*batteryLevel;
				bar.setLayoutParams(baseparam);
				
			
			}
    		
    	});
		if (true) {
			bar.setVisibility(View.VISIBLE);
			ViewGroup.LayoutParams baseparam = bar.getLayoutParams();
			width = baseparam.width;
			height = baseparam.height;
			int batdev = width / 100;
			//baseparam.
			//this.addView(bar);
			
		} else {
			//this.setVisibility(View.GONE);
		}
    }
    private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {

				batteryStatus = intent.getIntExtra(BatteryManager.EXTRA_STATUS,
						BatteryManager.BATTERY_STATUS_UNKNOWN);

				batteryLevel = intent.getIntExtra("level", 50);

			}

			appendText = Settings.System.getString(getBaseContext()
					.getContentResolver(), "battery_text_append");

			prependText = Settings.System.getString(getBaseContext()
					.getContentResolver(), "battery_text_prepend");

			style = Settings.System.getInt(getBaseContext().getContentResolver(),
					"battery_text_style", STYLE_DISABLE);

			//updatebar();
		}
	};
}
package com.pvy.quickoptions.dialogs;



import android.app.Dialog;
import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class volumeDialog extends Dialog implements android.view.View.OnClickListener {
	    private Context context;
		private SeekBar seekbarr;
		private SeekBar seekbarm;
		private SeekBar seekbara;
		private SeekBar seekbarn;
		private TextView note;
		private TextView ring;
		private TextView med;
		private TextView alrm;

	    public volumeDialog(Context context){
	        super(context);
	        this.context = context;
	    }
	 
	    /** Called when the activity is first created. */
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        this.setCancelable(true);
	        this.setTitle("Volume");
	        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(300, LayoutParams.WRAP_CONTENT);
	        LinearLayout.LayoutParams slideLP = new LinearLayout.LayoutParams(280, LayoutParams.WRAP_CONTENT);
	        slideLP.gravity = Gravity.CENTER;
	        seekbarr = new SeekBar(context);
	        ring = new TextView(context);
	        ring.setText("Ringer");
	        seekbarm = new SeekBar(context);
	        med = new TextView(context);
	        med.setText("Media");
	        seekbara = new SeekBar(context);
	        alrm = new TextView(context);
	        alrm.setText("Alarm");
	        seekbarn = new SeekBar(context);
	        note = new TextView(context);
	        note.setText("Notification");
	        seekbarn.setMax(255);
	        seekbarr.setMax(255);
	        seekbarm.setMax(255);
	        seekbara.setMax(255);
	        LinearLayout ll = new LinearLayout(context);
	        ll.setOrientation(LinearLayout.VERTICAL);
	        ll.addView(ring, slideLP);
	        ll.addView(seekbarr, slideLP);
	        ll.addView(note, slideLP);
	        ll.addView(seekbarn, slideLP);
	        ll.addView(alrm, slideLP);
	        ll.addView(seekbara, slideLP);
	        ll.addView(med, slideLP);
	        ll.addView(seekbarm, slideLP);      
	        addContentView(ll, lp);
	        
	        final AudioManager am = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);

	        //titled.setText("");
	    	seekbarr.setMax(am.getStreamMaxVolume(am.STREAM_RING));
	    	seekbarr.setProgress(am.getStreamVolume(am.STREAM_RING));
	    	seekbarr.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){
	    		@Override
	    		public void onProgressChanged(SeekBar seekBar, int progress,
	    				boolean fromUser) {
	    		    int index = seekBar.getProgress();
	    			am.setStreamVolume(am.STREAM_RING, index, 1);
	    		}
	    		@Override
	    		public void onStartTrackingTouch(SeekBar seekBar) {
	    			
	    		}
	    		@Override
	    		public void onStopTrackingTouch(SeekBar seekBar) {
	    			volumeDialog.this.cancel();
	    		}
	    		
	    	});
	    	seekbarn.setMax(am.getStreamMaxVolume(am.STREAM_NOTIFICATION));
	    	seekbarn.setProgress(am.getStreamVolume(am.STREAM_NOTIFICATION));
	    	seekbarn.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){
	    		@Override
	    		public void onProgressChanged(SeekBar seekBar, int progress,
	    				boolean fromUser) {
	    		    int index = seekBar.getProgress();
	    			am.setStreamVolume(am.STREAM_NOTIFICATION, index, 1);
	    		}
	    		@Override
	    		public void onStartTrackingTouch(SeekBar seekBar) {
	    			
	    		}
	    		@Override
	    		public void onStopTrackingTouch(SeekBar seekBar) {
	    			volumeDialog.this.cancel();
	    		}
	    		
	    	});
	    	seekbara.setMax(am.getStreamMaxVolume(am.STREAM_ALARM));
	    	seekbara.setProgress(am.getStreamVolume(am.STREAM_ALARM));
	    	seekbara.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){
	    		@Override
	    		public void onProgressChanged(SeekBar seekBar, int progress,
	    				boolean fromUser) {
	    		    int index = seekBar.getProgress();
	    			am.setStreamVolume(am.STREAM_ALARM, index, 1);
	    		}
	    		@Override
	    		public void onStartTrackingTouch(SeekBar seekBar) {
	    			
	    		}
	    		@Override
	    		public void onStopTrackingTouch(SeekBar seekBar) {
	    			volumeDialog.this.cancel();
	    		}
	    		
	    	});
	    	seekbarm.setMax(am.getStreamMaxVolume(am.STREAM_MUSIC));
	    	seekbarm.setProgress(am.getStreamVolume(am.STREAM_MUSIC));
	    	seekbarm.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){
	    		@Override
	    		public void onProgressChanged(SeekBar seekBar, int progress,
	    				boolean fromUser) {
	    		    int index = seekBar.getProgress();
	    			am.setStreamVolume(am.STREAM_MUSIC, index, 1);
	    		}
	    		@Override
	    		public void onStartTrackingTouch(SeekBar seekBar) {
	    			
	    		}
	    		@Override
	    		public void onStopTrackingTouch(SeekBar seekBar) {
	    			volumeDialog.this.cancel();
	    		}
	    		
	    	});
	    }
	   
		public void update(Integer totalfi, Integer curentfi, Integer totalsize, Integer curentsize, String filename) {
		    
	    }
	  
	    public void onClick(View v) {
	       
	    }
}

package com.pvy.quickoptions.dialogs;



import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class brightnessDialog extends Dialog implements android.view.View.OnClickListener {
	    private Context context;
		private SeekBar seekbarb;
		
	    public brightnessDialog(Context context){
	        super(context);
	        this.context = context;
			Log.d("pvyMods", "create");

	    }
	 
	    /** Called when the activity is first created. */
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
			Log.d("pvyMods", "onCreate");

	        //setContentView(R.layout.slider);
	        this.setTitle("brightness");
	        //titled.setText("");
	        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(300, LayoutParams.WRAP_CONTENT);
	        LinearLayout.LayoutParams ViewLP = new LinearLayout.LayoutParams(280, LayoutParams.WRAP_CONTENT);
	        seekbarb = new SeekBar(context);
	        seekbarb.setMax(255);
	        LinearLayout ll = new LinearLayout(context);
	        ll.setOrientation(LinearLayout.VERTICAL);
	        ll.addView(seekbarb, ViewLP);
	        addContentView(ll, lp);
	        
	        //seekbarb = (SeekBar) findViewById(R.id.bright);
	        int test = android.provider.Settings.System.getInt(this.context.getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS, 1);
	        seekbarb.setProgress(test);
	    	seekbarb.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){
	    		@Override
	    		public void onProgressChanged(SeekBar seekBar, int progress,
	    				boolean fromUser) {
	    		    int index = seekBar.getProgress();
	    		    if (index < 25){
	                	index = 26;
	                }
	    		    android.provider.Settings.System.putInt(context.getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS, index);
	                WindowManager.LayoutParams lp = getWindow().getAttributes();
	                
	                lp.screenBrightness = index / 255.0f;
	                getWindow().setAttributes(lp);
	    			//test1.setText(String.valueOf(index) + " "+ test );
	    		}
	    		@Override
	    		public void onStartTrackingTouch(SeekBar seekBar) {
	    		}
	    		@Override
	    		public void onStopTrackingTouch(SeekBar seekBar) {
	    			brightnessDialog.this.cancel();
	    		}
	    		
	    	});
	    	
	    }
	   
		public void update(Integer totalfi, Integer curentfi, Integer totalsize, Integer curentsize, String filename) {
		    
	    }
	  
	    public void onClick(View v) {
	       
	    }
}

package com.android.systemui.statusbar;

import com.android.systemui.statusbar.SignalText.SettingsObserver;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.os.BatteryManager;
import android.os.Handler;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

public class BatteryBar extends TextView {
	
	private int width;
	private int height;
	private int batteryLevel;
	private int batteryStatus;
	private boolean mAttached;
	   
	private static final int STYLE_SHOW = 1;
	private static final int STYLE_DISABLE = 2;

	private int style;

	public BatteryBar(Context context) {
		super(context);

	}

	public BatteryBar(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
		
	}

	public BatteryBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

	}
	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		this.setText(""+"");
		
		if (!mAttached) {
			mAttached = true;
			IntentFilter filter = new IntentFilter(); //setup my system intent filters
			filter.addAction(Intent.ACTION_BATTERY_CHANGED); // monitor battery
	        filter.addAction(Intent.ACTION_CONFIGURATION_CHANGED); // monitor system orientation to change width when in land / port mode
			getContext().registerReceiver(mIntentReceiver, filter, null,
					getHandler());
		    new SettingsObserver(getHandler()).observe();
		}
		updateBatteryBar();
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		if (mAttached) {
			getContext().unregisterReceiver(mIntentReceiver);
			mAttached = false;
		}
	}

	/**
	 * Handles changes ins battery level and charger connection
	 */
	private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {

				batteryStatus = intent.getIntExtra(BatteryManager.EXTRA_STATUS,
						BatteryManager.BATTERY_STATUS_UNKNOWN);
				batteryLevel = intent.getIntExtra("level", 100);
			}
			
			style = Settings.System.getInt(getContext().getContentResolver(),
					"battery_text_style", STYLE_DISABLE);
			
			BatteryBar.this.setText(""+"");
			updateBatteryBar();
		}
	};

	final void updateBatteryColor() {

		boolean autoColorBatteryText = Settings.System.getInt(getContext()
				.getContentResolver(), "battery_text_color", 1) == 1 ? true
				: false;

		int color_auto_charging = Settings.System.getInt(getContext()
				.getContentResolver(), "battery_color_auto_charging",
				0xFF93D500);

		int color_auto_regular = Settings.System
				.getInt(getContext().getContentResolver(),
						"battery_color_auto_regular", 0xFFFFFFFF);

		int color_auto_medium = Settings.System.getInt(getContext()
				.getContentResolver(), "battery_color_auto_medium", 0xFFD5A300);

		int color_auto_low = Settings.System.getInt(getContext()
				.getContentResolver(), "battery_color_auto_low", 0xFFD54B00);

		int color_regular = Settings.System.getInt(getContext()
				.getContentResolver(), "battery_color", 0xFFFFFFFF);

		if (autoColorBatteryText) {
			if (batteryStatus == BatteryManager.BATTERY_STATUS_CHARGING
					|| batteryStatus == BatteryManager.BATTERY_STATUS_FULL) {
				setBackgroundColor(color_auto_charging);
				
			} else {
				if (batteryLevel < 15) {
					setBackgroundColor(color_auto_low);
				} else if (batteryLevel < 40) {
					setBackgroundColor(color_auto_medium);
				} else {
					setBackgroundColor(color_auto_regular);
				}

			}
		} else {
			setBackgroundColor(color_regular);
		}
	}

	final void updateBatteryBar() {
		updateBatteryColor();
		style = Settings.System.getInt(getContext().getContentResolver(),
				"battery_bar_style", STYLE_DISABLE);
		int settingsHeight = Settings.System.getInt(getContext()
				.getContentResolver(), "battery_bar_height", 1);
		if (style == STYLE_SHOW) {
			this.setVisibility(View.VISIBLE);
			WindowManager screen = (WindowManager) this.getContext().getApplicationContext().getSystemService("window");
			ViewGroup.LayoutParams baseparam = getLayoutParams();
			width = screen.getDefaultDisplay().getWidth();
			height = settingsHeight;
			baseparam.width = ((batteryLevel*width)/100);
			baseparam.height = height;
			setLayoutParams(baseparam);
		} else {
			this.setVisibility(View.GONE);
		}
	}
	
	class SettingsObserver extends ContentObserver {
		public SettingsObserver(Handler handler) {
			super(handler);
			// TODO Auto-generated constructor stub
		}

		void observe() {
			Log.d("pvymods", "battery bar observer started");

			Context mContext = getContext();
			
			ContentResolver localContentResolver = mContext
					.getContentResolver();
			
			localContentResolver.registerContentObserver(
					Settings.System.getUriFor("battery_bar_style"),
					false, this);
			localContentResolver.registerContentObserver(
					Settings.System.getUriFor("battery_bar_height"),
					false, this);
			localContentResolver
					.registerContentObserver(Settings.System
							.getUriFor("battery_color"),
							false, this);
			localContentResolver
					.registerContentObserver(Settings.System
							.getUriFor("battery_color_auto_low"),
							false, this);
			localContentResolver.registerContentObserver(Settings.System
							.getUriFor("battery_color_auto_medium"),
							false, this);
			localContentResolver.registerContentObserver(
					Settings.System.getUriFor("battery_color_auto_regular"),
							false, this);
			localContentResolver.registerContentObserver(
					Settings.System.getUriFor("battery_color_auto_charging"),
					false, this);
			localContentResolver.registerContentObserver(
					Settings.System.getUriFor("battery_auto_color"),
					false, this);
			
		}

		public void onChange(boolean paramBoolean) {
			
			updateBatteryBar();
			Log.d("pvymods", "batterybar observed change done");
			
		}

	}

	
}

package com.android.systemui.statusbar;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.database.ContentObserver;
import android.graphics.Typeface;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.format.DateFormat;
import android.text.style.CharacterStyle;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class Date extends TextView {
    private boolean mAttached;
    private Calendar mCalendar;
	private boolean text;

    private String mClockFormatString;
    private SimpleDateFormat mClockFormat;
    private static final int SIZE_SML  = 0;
    private static final int SIZE_NML  = 1;
    private static final int SIZE_LRG  = 2;
    private static final int AM_PM_STYLE_NORMAL  = 0;
    private static final int AM_PM_STYLE_SMALL   = 1;
    private static final int AM_PM_STYLE_GONE    = 2;
    private static final int CLOCK_GONE    = 3;
    private float textsize = getTextSize()-4;

    private static int AM_PM_STYLE = AM_PM_STYLE_GONE;
    private String DATE_STYLE;
    
    private static int Text_Size = SIZE_NML;
    
    public Date(Context context) {
        this(context, null);
        Handler mHandler = new Handler();
	    new SettingsObserver(mHandler).observe();
    }

    public Date(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        Handler mHandler = new Handler();
	    new SettingsObserver(mHandler).observe();
    }

    public Date(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Handler mHandler = new Handler();
	    new SettingsObserver(mHandler).observe();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        if (!mAttached) {
            mAttached = true;
            IntentFilter filter = new IntentFilter();

            filter.addAction(Intent.ACTION_TIME_TICK);
            filter.addAction(Intent.ACTION_TIME_CHANGED);
            filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
            filter.addAction(Intent.ACTION_CONFIGURATION_CHANGED);

            getContext().registerReceiver(mIntentReceiver, filter, null, getHandler());
        }

        mCalendar = Calendar.getInstance(TimeZone.getDefault());
        
        //update the typeface
        updateTypeface();

        // Make sure we update to the current time
        updateClock();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mAttached) {
            getContext().unregisterReceiver(mIntentReceiver);
            mAttached = false;
        }
    }

    private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_TIMEZONE_CHANGED)) {
                String tz = intent.getStringExtra("time-zone");
                mCalendar = Calendar.getInstance(TimeZone.getTimeZone(tz));
                if (mClockFormat != null) {
                    mClockFormat.setTimeZone(mCalendar.getTimeZone());
                }
            }
            mClockFormatString = "";
            updateClock();
            
        }
    };

    final void updateClock() {
    	DATE_STYLE = Settings.System.getString(getContext().getContentResolver(), "tweaks_date_style");
    	Text_Size = Settings.System.getInt(getContext().getContentResolver(), "tweaks_date_size", 1);
    	int textface = Settings.System.getInt(getContext().getContentResolver(),
				"tweaks_date_text_font", 0);
		
		if (textface == 1){
			text = true;
		} else {
			text = false;
		}
		
        mCalendar.setTimeInMillis(System.currentTimeMillis());
        updateColor();
        updateTypeface();
        updateSize();
        setText(getSmallTime());
    }
    
    private final void updateColor() {
    	this.setTextColor(Settings.System.getInt(getContext().getContentResolver(), "tweaks_date_color", 0xffffffff));
    	this.refreshDrawableState();
    }
    private final void updateTypeface() {
    	if (text){
			String filelocation = "system/TEAM/font/";
			String fontfile = "clock_font.ttf";
			Log.d("pvymods", "signal " + filelocation + fontfile);
			File file = new File(filelocation, fontfile);
			Typeface myTypeface = Typeface.createFromFile(file);
			this.setTypeface(myTypeface);
		} else {
			Typeface noTypeface = Typeface.DEFAULT;
			this.setTypeface(noTypeface);
		}
    }
    private final void updateSize() {
    	if (Text_Size == SIZE_LRG){
    	setTextSize(textsize + 2);
    	}else if (Text_Size == SIZE_NML){
        	setTextSize(textsize);
    	}else if (Text_Size == SIZE_SML){
        	setTextSize(textsize - 2);
    	}
    }
    

    private final CharSequence getSmallTime() {
        Context context = getContext();
        
    	
    	if(AM_PM_STYLE == CLOCK_GONE) {
    		this.setVisibility(View.GONE);
    		return "";
    	} else {
    		this.setVisibility(View.VISIBLE);
    	}
        
        String res;
        res = "EEEE, MMMM dd, yyyy";
     
        SimpleDateFormat sdf;
        //String format = context.getString(res);
        String format = res;
        mClockFormat = sdf = new SimpleDateFormat(format);
        sdf = mClockFormat;
      
        String result = sdf.format(mCalendar.getTime());

        return result;

    }
    class SettingsObserver extends ContentObserver {
		public SettingsObserver(Handler handler) {
			super(handler);
			// TODO Auto-generated constructor stub
			
			Log.d("pvymods", "clock observer start");

		}

		void observe() {
			Log.d("pvymods", "clock observe");

			Context mContext = getContext();
			
			ContentResolver localContentResolver = mContext
					.getContentResolver();
			
			
			localContentResolver
					.registerContentObserver(Settings.System
							.getUriFor("tweaks_date_style"),
							false, this);
			localContentResolver
					.registerContentObserver(Settings.System
							.getUriFor("tweaks_date_color"),
							false, this);

			localContentResolver
					.registerContentObserver(Settings.System
							.getUriFor("tweaks_date_size"),
							false, this);
			
			localContentResolver
			.registerContentObserver(Settings.System
							.getUriFor("tweaks_date_text_font"),
							false, this);
		}

		public void onChange(boolean paramBoolean) {
			
			updateTypeface();
			updateClock();

			Log.d("pvymods", "clock observed change done");
			
		}

	}
}



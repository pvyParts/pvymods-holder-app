package com.android.systemui.statusbar;

import java.io.File;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.graphics.Typeface;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.CharacterStyle;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class SignalText extends TextView {
	int ASU = 0;
	int dBm = 0;

	private boolean mAttached;

	private boolean text;
	
	private String prependText = "";
	private String appendText = "dBm";
	private String APP_NAME = "pvymods";
	private static final int STYLE_SHOW = 1;
	private static final int STYLE_DISABLE = 0;
	private static final int STYLE_SMALL_DBM = 2;
	private static final int SIZE_SML  = 0;
    private static final int SIZE_NML  = 1;
    private static final int SIZE_LRG  = 2;
	
    private float textsize = getTextSize()-4;
	
    private static int Text_Size = SIZE_NML;

	private int style;
	private int type;

	public SignalText(Context context) {
		super(context);
		Log.d(APP_NAME, "start 100");
		TelephonyManager tm = (TelephonyManager) this.getContext()
				.getSystemService("phone");
		int events = PhoneStateListener.LISTEN_SIGNAL_STRENGTH;
		
		tm.listen(phoneStateListener, events);
		updateSignalSettings();
		Handler mHandler = new Handler();
	    new SettingsObserver(mHandler).observe();

		updateTypeface();
		updateSignalText();
	}

	public SignalText(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
		Log.d(APP_NAME, "start 110");

	}

	public SignalText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		Log.d(APP_NAME, "start 111");

		TelephonyManager tm = (TelephonyManager) this.getContext()
				.getSystemService("phone");
		int events = PhoneStateListener.LISTEN_SIGNAL_STRENGTH;
		tm.listen(phoneStateListener, events);
		updateSignalSettings();
		Handler mHandler = new Handler();
	    new SettingsObserver(mHandler).observe();
	    
		updateTypeface();
		updateSignalText();
	}
	   private final void updateSize() {
	    	Text_Size = Settings.System.getInt(getContext().getContentResolver(), "tweaks_sig_size", 1);
	    	if (Text_Size == SIZE_LRG){
	    	setTextSize(textsize + 2);
	    	}else if (Text_Size == SIZE_NML){
	        	setTextSize(textsize);
	    	}else if (Text_Size == SIZE_SML){
	        	setTextSize(textsize - 2);
	    	}
	    }

	private final PhoneStateListener phoneStateListener = new PhoneStateListener() {
		@Override
		public void onSignalStrengthChanged(int asu) {
			Log.d(APP_NAME, "onSignalStrengthChanged " + asu);
			ASU = asu;
			dBm = -113 + (2 * ASU);
			updateSignalSettings();
			updateSignalText();
			super.onSignalStrengthChanged(asu);
		}
	};

	private final void updateTypeface() {
		if (text){
			String filelocation = "system/TEAM/font/";
			String fontfile = "clock_font.ttf";
			Log.d("pvymods", "signal " + filelocation + fontfile);
			File file = new File(filelocation, fontfile);
			Typeface myTypeface = Typeface.createFromFile(file);
			this.setTypeface(myTypeface);
		} else {
			Typeface noTypeface = Typeface.DEFAULT;
			this.setTypeface(noTypeface);
		}
		
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		if (!mAttached) {
			mAttached = true;
			// Log.d("pvymods", "signal observed attached = " + mAttached);
		}
		prependText = "";
		updateSize();
		updateSignalSettings();
		updateTypeface();
		updateSignalText();
	}

	final void updateSignalText() {
		updateSize();
		int APM = Settings.System.getInt(getContext().getContentResolver(),
				"airplane_mode_on", -1);
		if (APM == 0){
		if (style == STYLE_SHOW) {
			// Log.d("pvymods", "signal show me");

			this.setVisibility(View.VISIBLE);

			String result = " "+Integer.toString(ASU)+" ";

			setText(result);

		} else if (style == STYLE_SMALL_DBM) {
			// Log.d("pvymods", "signal show me small");

			this.setVisibility(View.VISIBLE);

			String result = prependText + Integer.toString(dBm) + "dBm ";

			SpannableStringBuilder formatted = new SpannableStringBuilder(
					result);
			int start = result.indexOf("d");

			CharacterStyle style = new RelativeSizeSpan(0.7f);
			formatted.setSpan(style, start, start + 3,
					Spannable.SPAN_EXCLUSIVE_INCLUSIVE);

			setText(formatted);
		} else {
			// Log.d("pvymods", "signal hide me");
			this.setVisibility(View.GONE);
		}
		} else {
			setText("AP MODE");
		}
		if (ASU == -1){
			this.setVisibility(View.GONE);
		}
		
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		if (mAttached) {
			mAttached = false;
		}
	}

	final void updateSignalSettings() {

		Log.d("pvymods", "signal update");

		
		
		style = Settings.System.getInt(getContext().getContentResolver(),
				"tweaks_signal_text_style", STYLE_DISABLE);

		boolean autoColor = Settings.System.getInt(getContext()
				.getContentResolver(), "tweaks_signal_text_autocolor_enabled",
				1) == 1 ? true : false;

		int color_regular = Settings.System.getInt(getContext()
				.getContentResolver(), "tweaks_signal_text_color_static",
				0xFFFFFFFF);

		int color_0 = Settings.System.getInt(getContext().getContentResolver(),
				"tweaks_signal_text_color_0_bar", 0xFF93D500);

		int color_1 = Settings.System.getInt(getContext().getContentResolver(),
				"tweaks_signal_text_color_1_bar", 0xFFFFFFFF);

		int color_2 = Settings.System.getInt(getContext().getContentResolver(),
				"tweaks_signal_text_color_2_bar", 0xFFD5A300);

		int color_3 = Settings.System.getInt(getContext().getContentResolver(),
				"tweaks_signal_text_color_3_bar", 0xFFD54B00);

		int color_4 = Settings.System.getInt(getContext().getContentResolver(),
				"tweaks_signal_text_color_4_bar", 0xFFFFFFFF);
		
		int textface = Settings.System.getInt(getContext().getContentResolver(),
				"tweaks_signal_text_font", 0);
		
		if (textface == 1){
			text = true;
		} else {
			text = false;
		}
		
		if (autoColor) {
			if (ASU >= 12 ) {
				setTextColor(color_4);
			} else if (ASU >= 8 && ASU < 12) {
				setTextColor(color_3);
			} else if (ASU >= 5 && ASU < 8) {
				setTextColor(color_2);
			} else if (ASU >= 2 && ASU < 5) {
				setTextColor(color_1);
			} else if (ASU < 2) {
				setTextColor(color_0);
			} else {
				setTextColor(color_regular);
			}
		}
	}
	class SettingsObserver extends ContentObserver {
		public SettingsObserver(Handler handler) {
			super(handler);
			// TODO Auto-generated constructor stub
			
			Log.d("pvymods", "signal observe");

		}

		void observe() {
			Log.d("pvymods", "signal observe");

			Context mContext = getContext();
			
			ContentResolver localContentResolver = mContext
					.getContentResolver();
			localContentResolver.registerContentObserver(Settings.System
					.getUriFor("airplane_mode_on"),
					false, this);
			localContentResolver.registerContentObserver(
					Settings.System.getUriFor("tweaks_signal_text_style"),
					false, this);
			localContentResolver.registerContentObserver(
					Settings.System.getUriFor("tweaks_sig_size"),
					false, this);
			localContentResolver.registerContentObserver(Settings.System
					.getUriFor("tweaks_signal_text_autocolor_enabled"), false,
					this);
			localContentResolver.registerContentObserver(Settings.System
					.getUriFor("tweaks_signal_text_color_static"), false, this);
			localContentResolver
					.registerContentObserver(Settings.System
							.getUriFor("tweaks_signal_text_color_0_bar"),
							false, this);
			localContentResolver
					.registerContentObserver(Settings.System
							.getUriFor("tweaks_signal_text_color_1_bar"),
							false, this);
			localContentResolver
					.registerContentObserver(Settings.System
							.getUriFor("tweaks_signal_text_color_2_bar"),
							false, this);
			localContentResolver
					.registerContentObserver(Settings.System
							.getUriFor("tweaks_signal_text_color_3_bar"),
							false, this);
			localContentResolver
					.registerContentObserver(Settings.System
							.getUriFor("tweaks_signal_text_color_4_bar"),
							false, this);
			localContentResolver
			.registerContentObserver(Settings.System
					.getUriFor("tweaks_signal_text_font"),
					false, this);
		}

		public void onChange(boolean paramBoolean) {
			
			updateSignalSettings();
			updateTypeface();
			updateSignalText();

			Log.d("pvymods", "signal observed change done");
			
		}

	}

}
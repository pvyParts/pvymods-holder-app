/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/***************************************************************************
 *
 * 	                     PvyMods Carrier Label
 *     						  Version 0.01
 * 
 * 	              Written by Aaron Kable for TEAM roms
 *             Please inform us if you plan to use these 
 *                      sources as a courtesy.
 *                      
 *               Created to resemble HC statusbar info
 *           
 *      will show up to 4 lines of info with EVERYthing turned on
 *       not that you would want to but we shall see how it goes
 *           
 *           eg.
 *           ------------------------
 *           -    Custom String		-
 *           -  batt% 	Sig	  ALARM	-
 *           -  wifi / carier name	-
 *           -       phone#			-
 * 			 ------------------------
 * 
 *		might add options for alignment too eg left, right, centre.
 *
 * 
 * Additional options; 
 * 
 * 		battery %
 * 		signal strength
 * 		alarm time
 * 
 * 		wifi name
 *		network name
 * 		network number
 * 
 * 		Custom Font Select
 * 		Custom Font Size
 * 
 * Reverse Engineer;
 * 
 * 		AP mode
 * 		no signal
 * 
 * Need string id's for 
 * 
 * 		no sig
 * 		ap mode
 * 		searching
 * 		emergency only
 * 
 ***************************************************************************/

package com.android.systemui.statusbar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.provider.Telephony;
import android.telephony.TelephonyManager;
import android.util.AttributeSet;
import android.util.Slog;
import android.widget.TextView;

public class CarrierLabel extends TextView {
	private boolean mAttached;
	private Context mContext;
	
	private final int AP_MODE_ON = 1;
	private final int AP_MODE_OFF = 0;
	
	private boolean AP_MODE;
	private boolean WIFI_CON;
	private boolean DATA_CON;
	private boolean SIGNAL;

	private String networkName;
	private String wifiName;
	private int str_emergency = 0; //replace with systemUI.apk/Framework public id's
	private int str_nosig = 0; //replace with systemUI.apk/Framework public id's
	private int str_searching = 0; //replace with systemUI.apk/Framework public id's
	private int str_airplane = 0; //replace with systemUI.apk/Framework public id's
	
	// Default Text ect ect... 
	private String str_cust_text = "pvyMods Status View";
	
	// Hide or show ints
	// Layout
	private int STYLE_HIDE = 0;
	private int STYLE_SHOW = 1;
	// Font 
	private int FONT_HIDE = 0;
	private int FONT_SHOW = 1;
	// Battery %
	private int BATT_HIDE = 0;
	private int BATT_SHOW = 1;
	// Signal 
	private int SIGS_HIDE = 0;
	private int SIGS_SHOW = 1;
	// WiFi
	private int WIFI_HIDE = 0;
	private int WIFI_SHOW = 1;
	// Custon String
	private int CUST_HIDE = 0;
	private int CUST_SHOW = 1;
	
	// Options
	// new line string ( because of some issues i was having
	private String LN = "\n";
	
	public CarrierLabel(Context context) {
		this(context, null);
		
	}

	public CarrierLabel(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public CarrierLabel(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		updateNetworkName(false, null, false, null);
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();

		if (!mAttached) {
			mAttached = true;
			IntentFilter filter = new IntentFilter();
			filter.addAction(Telephony.Intents.SPN_STRINGS_UPDATED_ACTION);
			getContext().registerReceiver(mIntentReceiver, filter, null,
					getHandler());
		}
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		if (mAttached) {
			getContext().unregisterReceiver(mIntentReceiver);
			mAttached = false;
		}
	}

	private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (Telephony.Intents.SPN_STRINGS_UPDATED_ACTION.equals(action)) {
				updateNetworkName(intent.getBooleanExtra(
						Telephony.Intents.EXTRA_SHOW_SPN, false),
						intent.getStringExtra(Telephony.Intents.EXTRA_SPN),
						intent.getBooleanExtra(
								Telephony.Intents.EXTRA_SHOW_PLMN, false),
						intent.getStringExtra(Telephony.Intents.EXTRA_PLMN));
			}
		}
	};

	void updateNetworkName(boolean showSpn, String spn, boolean showPlmn,
			String plmn) {
		
		//check flight mode 
		int APM = Settings.System.getInt(getContext().getContentResolver(),
				"airplane_mode_on", -1);
		
		if (APM == AP_MODE_ON){
			AP_MODE = true;
		} else {
			AP_MODE = false;
		}
		/*setup the network monitors :D yeah there are a few to get all the details i want 
		 * 
		 * connManager for the mobile and wifi network connection states
		 * 
		 * mWifi and mMobile for connection tests on the networks
		 * 
		 * wifiManager for the name of the wifi network
		 * 
		 * teleManager for the operator name
		 * 
		 */
		
		
		ConnectivityManager connManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
		TelephonyManager teleManager =((TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE));
		NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mMobile= connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		
		// get the wifi details
		WIFI_CON = mWifi.isConnected();
		wifiName = (wifiManager.getConnectionInfo()).getSSID();
		// get the mobile network details
		DATA_CON = mMobile.isConnected();
		networkName = teleManager.getSimOperatorName();
		
		// get signal strength for what ever is connected
		if (WIFI_CON){
			
		}
		if (DATA_CON){
			
		}
		// get the custom string
		
		// get battery details
		
		// get alarm details
		
		
		/*make me my string!
		 *  ------------------------
		 *  -    Custom String		-
		 *  -  batt% 	Sig	  ALARM	-
		 *  -  wifi / carier name	-
		 *  -       phone#			-
		 *  ------------------------
		 */
		
		StringBuilder str = new StringBuilder();
		boolean signal = false;
		if (showPlmn && plmn != null) {
			str.append(plmn);
			signal = true;
		}
		if (showSpn && spn != null) {
			if (signal) {
				str.append(LN);
			}
			str.append(spn);
			signal = true;
		}
		if (signal) {
			setText(str.toString());
		} else {
			setText(0x10402b1);
		}

	}

}

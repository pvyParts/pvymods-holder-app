package com.android.systemui.statusbar.quickpanel;
/*
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class QuickSettingsView extends LinearLayout
{
  public QuickSettingsView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public int getSuggestedMinimumHeight()
  {
    return 0;
  }

  protected void onFinishInflate()
  {
    super.onFinishInflate();
  }
}
 */

import com.android.systemui.statusbar.quickpanel.*;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.provider.Settings.SettingNotFoundException;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class QuickSettingsView extends LinearLayout {

	/*private static final int ROTATION = 0;
    private static final int WIFI = 0;
    private static final int BT = 0;
    private static final int MOBILE_NETWORK = 0;
    private static final int BRIGHTNESS = 0;
    private static final int HOTSPOT = 0;
    private static final int SETTINGS = 0;
    private static final int GPS = 0;
    private static final int VOLUME_RINGTONE = 0;
    private static final int TASK_MANAGER = 0;
    private static final int VOLUME = 0;
    private static final int VIBRATION = 0;
    private static final int TORCH = 0;

    private static final int ITEM_NUMBER = 0;*/

    static final String TAG = "QuickSettings";

    private Context mContext;
    
    // options for the prefs
    
    private static final int QUICK_PREF_HIDE = 0;
    private static final int QUICK_PREF_SHOW = 1;

    // View id's for the quick pannel
    
	private int ref_quick_layout = 0x7f0a002a;
    
	private int ref_quick = 0x7f0a0016;
	
	private int ref_wifi = 0x7f0a002a;
	private int ref_bt = 0x7f0a002b;
	private int ref_gps = 0x7f0a002c;
	private int ref_volume = 0x7f0a0043;
	private int ref_bright = 0x7f0a003f;
	private int ref_rotate = 0x7f0a002e;
	private int ref_data = 0x7f0a002f;
	private int ref_flight = 0x7f0a0039;
	private int ref_autosync = 0x7f0a003a;
	
	// holders
	
	private QuickSettingButton wifi;
	private QuickSettingButton bt;
	private QuickSettingButton gps;
	private QuickSettingButton volume;
	private QuickSettingButton bright;
	private QuickSettingButton rotate;
	private QuickSettingButton data;
	private QuickSettingButton flight;
	private QuickSettingButton autosync;
	
    private Handler mHandler = null;
    
    public QuickSettingsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        
    	Log.d("pvymods", "quickView attached1!");

        mHandler = new Handler();
        SettingsObserver settingsObserver = new SettingsObserver(mHandler);
        settingsObserver.observe();
        
        wifi = ((QuickSettingButton) findViewById(ref_wifi));
		bt = (QuickSettingButton) findViewById(ref_bt);
		gps = (QuickSettingButton) findViewById(ref_gps);
		volume = (QuickSettingButton) findViewById(ref_volume);
		bright = (QuickSettingButton) findViewById(ref_bright);
		rotate = (QuickSettingButton) findViewById(ref_rotate);
		data = (QuickSettingButton) findViewById(ref_data);
		flight = (QuickSettingButton) findViewById(ref_flight);
		autosync = (QuickSettingButton) findViewById(ref_autosync);
		
    }

    private void init() {
    	Log.d("pvymods", "quickView attached2!");
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        int vis = -1;
		vis = android.provider.Settings.System.getInt(mContext.getContentResolver(), "quick_pref_vis", QUICK_PREF_SHOW);
    	String itemvis;
    	itemvis = android.provider.Settings.System.getString(mContext.getContentResolver(), "quick_pref_itemvis");
    	if (itemvis == null){
    		itemvis = "1|1|1|1|1|1|1|1|1";
    	}
    	Log.d("pvymods", "quickView found prefs!");
    	
		if (vis == QUICK_PREF_SHOW){
			this.setVisibility(View.VISIBLE);
	    	Log.d("pvymods", "quickView applied show!");
		} else if ( vis == QUICK_PREF_HIDE){
			this.setVisibility(View.GONE);
	    	Log.d("pvymods", "quickView applied hide!");
		} else if (vis == -1){
	    	Log.d("pvymods", "quickView FAILZOR!");
		}       
		
		String[] separated = itemvis.split("\\|");
    	Log.d("pvymods", "quickView "+itemvis);
    	
        /*if (Integer.parseInt(separated[0]) == 1){
			wifi.setVisibility(View.VISIBLE);
		} else{
			wifi.setVisibility(View.GONE);
		}
		if (Integer.parseInt(separated[1]) == 1){
			bt.setVisibility(View.VISIBLE);
		} else{
			bt.setVisibility(View.GONE);
		}
		if (Integer.parseInt(separated[2]) == 1){
			gps.setVisibility(View.VISIBLE);
		} else{
			gps.setVisibility(View.GONE);
		}
		if (Integer.parseInt(separated[3]) == 1){
			volume.setVisibility(View.VISIBLE);
		} else{
			volume.setVisibility(View.GONE);
		}
		if (Integer.parseInt(separated[4]) == 1){
			bright.setVisibility(View.VISIBLE);
		} else{
			bright.setVisibility(View.GONE);
		}
		if (Integer.parseInt(separated[5]) == 1){
			rotate.setVisibility(View.VISIBLE);
		} else{
			rotate.setVisibility(View.GONE);
		}
		if (Integer.parseInt(separated[6]) == 1){
			data.setVisibility(View.VISIBLE);
		} else{
			data.setVisibility(View.GONE);
		}
		if (Integer.parseInt(separated[7]) == 1){
			flight.setVisibility(View.VISIBLE);
		} else{
			flight.setVisibility(View.GONE);
		}
		if (Integer.parseInt(separated[8]) == 1){
			autosync.setVisibility(View.VISIBLE);
		} else{
			autosync.setVisibility(View.GONE);
		}*/
        
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

     public void updateVisibility() {
    	
    	 int vis = -1;
		vis = android.provider.Settings.System.getInt(mContext.getContentResolver(), "quick_pref_vis", QUICK_PREF_SHOW);
    	String itemvis;
    	itemvis = android.provider.Settings.System.getString(mContext.getContentResolver(), "quick_pref_itemvis");
    	if (itemvis == null){
    		itemvis = "1|1|1|1|1|1|1|1|1";
    	}
    	Log.d("pvymods", "quickView found prefs!");
    	
		if (vis == QUICK_PREF_SHOW){
			this.setVisibility(View.VISIBLE);
	    	Log.d("pvymods", "quickView applied show!");
		} else if ( vis == QUICK_PREF_HIDE){
			this.setVisibility(View.GONE);
	    	Log.d("pvymods", "quickView applied hide!");
		} else if (vis == -1){
	    	Log.d("pvymods", "quickView FAILZOR!");
		}       
		String[] separated = itemvis.split("\\|");
    	Log.d("pvymods", "quickView "+itemvis);
    	
        /*if (Integer.parseInt(separated[0]) == 1){
			wifi.setVisibility(View.VISIBLE);
		} else{
			wifi.setVisibility(View.GONE);
		}
		if (Integer.parseInt(separated[1]) == 1){
			bt.setVisibility(View.VISIBLE);
		} else{
			bt.setVisibility(View.GONE);
		}
		if (Integer.parseInt(separated[2]) == 1){
			gps.setVisibility(View.VISIBLE);
		} else{
			gps.setVisibility(View.GONE);
		}
		if (Integer.parseInt(separated[3]) == 1){
			volume.setVisibility(View.VISIBLE);
		} else{
			volume.setVisibility(View.GONE);
		}
		if (Integer.parseInt(separated[4]) == 1){
			bright.setVisibility(View.VISIBLE);
		} else{
			bright.setVisibility(View.GONE);
		}
		if (Integer.parseInt(separated[5]) == 1){
			rotate.setVisibility(View.VISIBLE);
		} else{
			rotate.setVisibility(View.GONE);
		}
		if (Integer.parseInt(separated[6]) == 1){
			data.setVisibility(View.VISIBLE);
		} else{
			data.setVisibility(View.GONE);
		}
		if (Integer.parseInt(separated[7]) == 1){
			flight.setVisibility(View.VISIBLE);
		} else{
			flight.setVisibility(View.GONE);
		}
		if (Integer.parseInt(separated[8]) == 1){
			autosync.setVisibility(View.VISIBLE);
		} else{
			autosync.setVisibility(View.GONE);
		}*/

		
		
    }
    
    class SettingsObserver extends ContentObserver {
        SettingsObserver(Handler handler) {
            super(handler);
        	Log.d("pvymods", "quickView ready to recieve!");

        }

        void observe() {
            ContentResolver resolver = mContext.getContentResolver();
             //android.provider.Settings.System
            resolver.registerContentObserver(android.provider.Settings.System.getUriFor("quick_pref_vis"),false, this);
            resolver.registerContentObserver(android.provider.Settings.System.getUriFor("quick_pref_itemvis"),false, this);

        }

        @Override
        public void onChange(boolean selfChange) {
        	Log.d("pvymods", "quickView recieved your request!");
        	updateVisibility();
        }
    }
}
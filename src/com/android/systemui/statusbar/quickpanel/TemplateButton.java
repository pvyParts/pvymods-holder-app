package com.android.systemui.statusbar.quickpanel;

import com.pvy.quickoptions.dialogs.*;

import android.app.PendingIntent;
import android.app.StatusBarManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public final class TemplateButton extends QuickSettingButton {
	private Context mContext = getContext();
	
	private brightnessDialog progressDialog2;

	public TemplateButton(Context paramContext, AttributeSet paramAttributeSet) {
		super(paramContext, paramAttributeSet);
	}

	private void updateIcons() {
		int i = 0;
		int j = 0;
		
		View localView = getRootView();
		
		ImageView localImageView1 = (ImageView) localView
				.findViewById(0x7f0a003c);//new icon layout id
		ImageView localImageView2 = (ImageView) localView
				.findViewById(0x7f0a003e);//new toggle layout id
		
			i = 0x7f020080;//new drawable id;
			j = 0x7f02007f;//new drawable id;
		
		localImageView1.setImageResource(i);
		localImageView2.setImageResource(j);
		return;

	}

	private void updateStatus() {
			setActivateStatus(1);
			updateIcons();
			return;
		
	}

	public void activate() {
		Log.e("pvyMods", "activate()");
		progressDialog2 = new brightnessDialog(mContext);
		progressDialog2.setCancelable(true);
		progressDialog2.show();
		//updateStatus();
	}

	public void deactivate() {
		Log.e("pvyMods", "deactivate()");
		//updateStatus();
	}

	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		Log.e("pvyMods", "onAttachedToWindow()");
		
		updateStatus();
	}

	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		Log.e("pvyMods", "onDetachedFromWindow()");
		
	}

	public boolean onLongClick(View paramView) {
		Intent localIntent = new Intent("android.intent.action.MAIN");
		localIntent.addCategory("android.intent.category.DEFAULT");
		// what am i mean to to open :D
		localIntent.setComponent(new ComponentName("com.android.phone",
				"com.android.phone.Settings"));
		localIntent.setFlags(268435456);
		PendingIntent localPendingIntent = PendingIntent.getActivity(
				this.mContext, 0, localIntent, 134217728);
		try {
			localPendingIntent.send();
		} catch (PendingIntent.CanceledException localCanceledException) {
		}
		((StatusBarManager) this.mContext.getSystemService("statusbar"))
				.collapse();
		return true;
	}

	public void updateResources() {
		// set to the string ID
		setText(0x7f080016);
	}
}


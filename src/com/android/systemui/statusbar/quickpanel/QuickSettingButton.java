package com.android.systemui.statusbar.quickpanel;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.StatusBarManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.TextView;

public abstract class QuickSettingButton extends TextView
  implements View.OnClickListener, View.OnLongClickListener
{
  private Context mContext = getContext();
  protected String mAction = "android.settings.SETTINGS";
  private int mActivateStatus;
  private BroadcastReceiver mIntentReceiver = new BroadcastReceiver()
  {
    public void onReceive(Context paramContext, Intent paramIntent)
    {
      QuickSettingButton.this.updateResources();
    }
  };
  private View mRootView = null;

  public QuickSettingButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  abstract void activate();

  abstract void deactivate();

  protected int getActivateStatus()
  {
    return this.mActivateStatus;
  }

  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    Log.e("QuickSettingButton", "onAttachedToWindow()");
    this.mRootView = getRootView();
    setOnClickListener(this);
    setOnLongClickListener(this);
    this.mContext.registerReceiver(this.mIntentReceiver, new IntentFilter("android.intent.action.CONFIGURATION_CHANGED"), null, null);
  }

  public void onClick(View paramView)
  {
    Log.d("pvyMods", "QuickSettingButton onClick activated!");
    setEnabled(false);
    if (1 == this.mActivateStatus)
      deactivate();
    while (true)
    {
    	if (this.mActivateStatus != 0)
           continue;
    	activate();
    	setEnabled(true);
    	return;
    }
  }

  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    Log.e("QuickSettingButton", "onDetachedFromWindow()");
  }

  public boolean onLongClick(View paramView)
  {
    Log.d("pvyMods", "QuickSettingButton Sending intent!");
    Intent localIntent = new Intent(this.mAction);
    localIntent.setFlags(268435456);
    PendingIntent localPendingIntent = PendingIntent.getActivity(this.mContext, 0, localIntent, 134217728);
    try
    {
      localPendingIntent.send();
    }
    catch (PendingIntent.CanceledException localCanceledException)
    {
    }
    ((StatusBarManager)this.mContext.getSystemService("statusbar")).collapse();
    return true;
  }

  protected void setActivateStatus(int paramInt)
  {
    this.mActivateStatus = paramInt;
  }

  abstract void updateResources();
}

/* Location:           C:\Users\Aaron Kable\Desktop\Java dcompiler\SystemUI_dex2jar.jar
 * Qualified Name:     com.android.systemui.statusbar.quickpanel.QuickSettingButton
 * JD-Core Version:    0.6.0
 */
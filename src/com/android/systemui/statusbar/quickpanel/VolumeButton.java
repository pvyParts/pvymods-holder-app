package com.android.systemui.statusbar.quickpanel;

import com.pvy.quickoptions.dialogs.*;

import android.app.PendingIntent;
import android.app.StatusBarManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.os.Handler;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public final class VolumeButton extends QuickSettingButton {
	private Context mContext = getContext();
	

	public VolumeButton(Context paramContext, AttributeSet paramAttributeSet) {
		super(paramContext, paramAttributeSet);
		
	}

	private void updateIcons() {
		int i = 0;
		int j = 0;
		
		View localView = getRootView();
		
		ImageView localImageView1 = (ImageView) localView
				.findViewById(0x7f0a003c);//new icon layout id
		ImageView localImageView2 = (ImageView) localView
				.findViewById(0x7f0a003e);//new toggle layout id
		
			i = 0x7f02009a;//new drawable id;
			j = 0x7f02007f;//new drawable id;
		
		localImageView1.setImageResource(i);
		localImageView2.setImageResource(j);
		return;

	}

	private void updateStatus() {
			setActivateStatus(1);
			updateIcons();
			return;
		
	}

	public void activate() {
		Log.e("pvyMods", "activate()");
		//Log.d("pvyMods", "" + progressDialog2.getOwnerActivity());

		//updateStatus();
	}

	public void deactivate() {
		Log.e("pvyMods", "deactivate()");
		
		Intent localIntent = new Intent("android.intent.action.MAIN");
		localIntent.addCategory("android.intent.category.DEFAULT"); // what am i mean to to open :D
		localIntent.setComponent(new ComponentName("com.pvy.quickoptions.dialogs",
				"com.pvy.quickoptions.dialogs.VolumeActivity"));
		localIntent.setFlags(268435456);
		PendingIntent localPendingIntent = PendingIntent.getActivity(
				this.mContext, 0, localIntent, 134217728);
		try {
			localPendingIntent.send();
		} catch (PendingIntent.CanceledException localCanceledException) {
			
		}
		((StatusBarManager) this.mContext.getSystemService("statusbar"))
				.collapse();

	}

	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		Log.e("pvyMods", "onAttachedToWindow()");
		Handler mHandler = new Handler();
	    new SettingsObserver(mHandler).observe();
		//Log.d("pvyMods", "" + progressDialog2.getOwnerActivity());
		updateStatus();
	}

	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		Log.e("pvyMods", "onDetachedFromWindow()");
		
	}

	public boolean onLongClick(View paramView) {
		Intent localIntent = new Intent("android.intent.action.MAIN");
		localIntent.addCategory("android.intent.category.DEFAULT");
		// what am i mean to to open :D
		localIntent.setComponent(new ComponentName("com.android.settings",
				"com.android.settings.SoundSettings"));
		localIntent.setFlags(268435456);
		PendingIntent localPendingIntent = PendingIntent.getActivity(
				this.mContext, 0, localIntent, 134217728);
		try {
			localPendingIntent.send();
		} catch (PendingIntent.CanceledException localCanceledException) {
		}
		((StatusBarManager) this.mContext.getSystemService("statusbar"))
				.collapse();
		return true;
	}
	class SettingsObserver extends ContentObserver {
		
		public SettingsObserver(Handler handler) {
			super(handler);
			// TODO Auto-generated constructor stub
			
			Log.d("pvymods", "brightness observe");
		}

		void observe() {
			Log.d("pvymods", "brightness observe");

			Context mContext = getContext();
			
			ContentResolver localContentResolver = mContext
					.getContentResolver();
					localContentResolver.registerContentObserver(Settings.System.getUriFor(android.provider.Settings.System.SCREEN_BRIGHTNESS),false, this);
					localContentResolver.registerContentObserver(Settings.System.getUriFor(android.provider.Settings.System.SCREEN_BRIGHTNESS_MODE), false, this);			
				}

		public void onChange(boolean paramBoolean) {
			updateStatus();
			Log.d("pvymods", "brightness changed");
			
		}

	}
	public void updateResources() {
		// set to the string ID
		setText("Volume");
	}
}


package com.android.systemui.statusbar.quickpanel;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.StatusBarManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public final class MobileDataSettingButton extends QuickSettingButton
{
	  private Context mContext = getContext();
private MobileDataObserver mMobileDataObserver = new MobileDataObserver();

  public MobileDataSettingButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  private void setMobileDataEnabled(boolean paramBoolean)
  {
    ConnectivityManager localConnectivityManager = (ConnectivityManager)this.mContext.getSystemService("connectivity");
    if (localConnectivityManager != null)
    {
      Log.i("MobileDataSettingButton", "setMobileDataEnabled: set to  = " + paramBoolean);
      //localConnectivityManager.setMobileDataEnabled(paramBoolean);
    }
    while (true)
    {
      Log.i("MobileDataSettingButton", "setMobileDataEnabled : connectivityManager = null");
      return;
    }
  }

  private void updateIcons()
  {
    int i = 0;
    int j = 0;
    View localView = getRootView();
    ImageView localImageView1 = (ImageView)localView.findViewById(2131361831);
    ImageView localImageView2 = (ImageView)localView.findViewById(2131361833);
    switch (getActivateStatus())
    {
    default:
    case 1:
    case 0:
    }
    while (true)
    {
      localImageView1.setImageResource(i);
      localImageView2.setImageResource(j);
      return;
      //i = 2130837652;
      //j = 2130837631;
      //continue;
      //i = 2130837651;
      //j = 2130837648;
    }
  }

  private void updateStatus()
  {
    if (1 == Settings.Secure.getInt(this.mContext.getContentResolver(), "mobile_data", 1))
      setActivateStatus(1);
    while (true)
    {
      updateIcons();
      setActivateStatus(0);
      return;
    }
  }

  public void activate()
  {
    Log.e("MobileDataSettingButton", "activate()");
    setMobileDataEnabled(true);
    updateStatus();
  }

  public void deactivate()
  {
    Log.e("MobileDataSettingButton", "deactivate()");
    setMobileDataEnabled(false);
    updateStatus();
  }

  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    Log.e("MobileDataSettingButton", "onAttachedToWindow()");
    this.mContext.getContentResolver().registerContentObserver(Settings.Secure.getUriFor("mobile_data"), false, this.mMobileDataObserver);
    updateStatus();
  }

  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    Log.e("MobileDataSettingButton", "onDetachedFromWindow()");
    this.mContext.getContentResolver().unregisterContentObserver(this.mMobileDataObserver);
  }

  public boolean onLongClick(View paramView)
  {
    Intent localIntent = new Intent("android.intent.action.MAIN");
    localIntent.addCategory("android.intent.category.DEFAULT");
    localIntent.setComponent(new ComponentName("com.android.phone", "com.android.phone.Settings"));
    localIntent.setFlags(268435456);
    PendingIntent localPendingIntent = PendingIntent.getActivity(this.mContext, 0, localIntent, 134217728);
    try
    {
      localPendingIntent.send();
    }
    catch (PendingIntent.CanceledException localCanceledException)
    {
    }
    ((StatusBarManager)this.mContext.getSystemService("statusbar")).collapse();
    return true;
  }

  public void updateResources()
  {
    setText(2131230739);
  }

  private class MobileDataObserver extends ContentObserver
  {
    public MobileDataObserver()
    {
      super(null);
    }

    public void onChange(boolean paramBoolean)
    {
      MobileDataSettingButton.this.updateStatus();
    }
  }
}

/* Location:           C:\Users\Aaron Kable\Desktop\Java dcompiler\SystemUI_dex2jar.jar
 * Qualified Name:     com.android.systemui.statusbar.quickpanel.MobileDataSettingButton
 * JD-Core Version:    0.6.0
 */
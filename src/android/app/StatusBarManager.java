/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app;

import android.content.Context;
import android.os.Binder;
import android.os.RemoteException;
import android.os.IBinder;
import android.os.ServiceManager;
import android.util.Slog;
import android.view.View;


/**
 * Allows an app to control the status bar.
 *
 * @hide
 */
public class StatusBarManager {

    public static final int DISABLE_EXPAND = View.DRAWING_CACHE_QUALITY_AUTO;
    public static final int DISABLE_NOTIFICATION_ICONS = View.DRAWING_CACHE_QUALITY_AUTO;
    public static final int DISABLE_NOTIFICATION_ALERTS
            = View.DRAWING_CACHE_QUALITY_AUTO;
    public static final int DISABLE_NOTIFICATION_TICKER
            = View.DRAWING_CACHE_QUALITY_AUTO;
    public static final int DISABLE_SYSTEM_INFO = View.DRAWING_CACHE_QUALITY_AUTO;
    public static final int DISABLE_HOME = View.DRAWING_CACHE_QUALITY_AUTO;
    public static final int DISABLE_RECENT = View.DRAWING_CACHE_QUALITY_AUTO;
    public static final int DISABLE_BACK = View.DRAWING_CACHE_QUALITY_AUTO;
    public static final int DISABLE_CLOCK = View.DRAWING_CACHE_QUALITY_AUTO;

    @Deprecated
    public static final int DISABLE_NAVIGATION = 
            View.DRAWING_CACHE_QUALITY_AUTO | View.DRAWING_CACHE_QUALITY_AUTO;

    public static final int DISABLE_NONE = 0x00000000;

    public static final int DISABLE_MASK = DISABLE_EXPAND | DISABLE_NOTIFICATION_ICONS
            | DISABLE_NOTIFICATION_ALERTS | DISABLE_NOTIFICATION_TICKER
            | DISABLE_SYSTEM_INFO | DISABLE_RECENT | DISABLE_HOME | DISABLE_BACK | DISABLE_CLOCK;

    private Context mContext;
    //private IStatusBarService mService;
    private IBinder mToken = new Binder();

    StatusBarManager(Context context) {
        mContext = context;
    }

   

    /**
     * Disable some features in the status bar.  Pass the bitwise-or of the DISABLE_* flags.
     * To re-enable everything, pass {@link #DISABLE_NONE}.
     */
    public void disable(int what) {
     
    }
    
    /**
     * Expand the status bar.
     */
    public void expand() {
    
    }
    
    /**
     * Collapse the status bar.
     */
    public void collapse() {
     
    }

    public void setIcon(String slot, int iconId, int iconLevel, String contentDescription) {
       
    }

    public void removeIcon(String slot) {
      
    }

    public void setIconVisibility(String slot, boolean visible) {
     
    }
}
